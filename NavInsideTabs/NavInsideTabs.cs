﻿using System;

using Xamarin.Forms;

namespace NavInsideTabs
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			var tabbedPage = new TabbedPage ();
			for (int i = 0; i < 5; ++i) {
				var contentPage = new ContentPage {
					Title = i.ToString (),
					Content = new StackLayout {
						VerticalOptions = LayoutOptions.Center,
						Children = {
							new Label {
								Text = "Page " + i.ToString ()
							}
						}
					}

				};
				tabbedPage.Children.Add (new NavigationPage (contentPage) {
					Title = i.ToString (),	
				});
			}
			MainPage = tabbedPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

