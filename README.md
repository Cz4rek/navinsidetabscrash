# Navigation Page inside Tab Page Crashes On Android #

This repository contains an example minimalist project that will crash on Android. The crash occurs, when switching between tabs a few times, usually the 2nd tab change is enough, like 0 -> 1 -> 2.

## Setup

The key thing in this setup is using the `FormsAppCompatActivity` as the base activity class, thus using the Android Support Libraries and `Theme.AppCompat`.

When the above is true and we put the Navigation Page as a child of the Tabbed Page, then crashes start on Android. This will not happen, when using `FormsApplicationActivity`: the same exact setup and using the `Holo` or `Material` theme will not incur crashes.

The offending pages setup can be viewed below.

```
#!c#

		var tabbedPage = new TabbedPage ();
		for (int i = 0; i < 5; ++i) {
			var contentPage = new ContentPage {
				Title = i.ToString (),
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							Text = "Page " + i.ToString ()
						}
					}
				}
			};
			tabbedPage.Children.Add (new NavigationPage (contentPage) {
				Title = i.ToString (),	
			});
		}
		MainPage = tabbedPage;
```


## Possible cause

StackOverflow suggests, that the `Recursive entry to executePendingTransactions` error is caused by wrongly setting up Fragments within Fragments (when one Fragment is inside another).

The solution seems to be using the `getChildFragmentManager()` instead `getFragmentManager()` when creating `Fragment`s that are embedded inside another `Fragment`.

Stack posts relating to this:

 * [ViewPager: Recursive entry to executePendingTransactions](http://stackoverflow.com/questions/7338823/viewpager-recursive-entry-to-executependingtransactions)
 * [Recursive entry to executePendingTransactions](http://stackoverflow.com/questions/22150950/recursive-entry-to-executependingtransactions)
 * [Fragment Recursive entry to executePendingTransactions Error](http://stackoverflow.com/questions/15151698/fragment-recursive-entry-to-executependingtransactions-error)
 * [Calling getSupportFragmentManager() after getChildFragmentManager() causes IllegalStateException: Recursive entry to executePendingTransactions](http://stackoverflow.com/questions/27985636/calling-getsupportfragmentmanager-after-getchildfragmentmanager-causes-illeg)

## Stacktrace

```
#!#c
[MonoDroid] UNHANDLED EXCEPTION:
[MonoDroid] Java.Lang.IllegalStateException: Recursive entry to executePendingTransactions
[MonoDroid] at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw () [0x0000c] in /Users/builder/data/lanes/3053/a94a03b5/source/mono/external/referencesource/mscorlib/system/runtime/exceptionservices/exceptionservicescommon.cs:143
[MonoDroid] at Android.Runtime.JNIEnv.CallBooleanMethod (IntPtr jobject, IntPtr jmethod) [0x00063] in /Users/builder/data/lanes/3053/a94a03b5/source/monodroid/src/Mono.Android/src/Runtime/JNIEnv.g.cs:218
[MonoDroid] at Android.Support.V4.App.FragmentManagerInvoker.ExecutePendingTransactions () [0x00033] in <filename unknown>:0
[MonoDroid] at Xamarin.Forms.Platform.Android.AppCompat.NavigationPageRenderer.Dispose (Boolean disposing) [0x00083] in <filename unknown>:0
[MonoDroid] at Java.Lang.Object.Dispose () [0x00000] in /Users/builder/data/lanes/3053/a94a03b5/source/monodroid/src/Mono.Android/src/Java.Lang/Object.cs:115
[MonoDroid] at Xamarin.Forms.Platform.Android.AppCompat.FragmentContainer.OnDestroyView () [0x00066] in <filename unknown>:0
[MonoDroid] at Android.Support.V4.App.Fragment.n_OnDestroyView (IntPtr jnienv, IntPtr native__this) [0x00009] in <filename unknown>:0
[MonoDroid] at (wrapper dynamic-method) System.Object:66a26012-a74f-4217-bec4-f45e177742c8 (intptr,intptr)
[MonoDroid] --- End of managed exception stack trace ---
[MonoDroid] java.lang.IllegalStateException: Recursive entry to executePendingTransactions
[MonoDroid] at android.support.v4.app.FragmentManagerImpl.execPendingActions(FragmentManager.java:1544)
[MonoDroid] at android.support.v4.app.FragmentManagerImpl.executePendingTransactions(FragmentManager.java:545)
[MonoDroid] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.n_onDestroyView(Native Method)
[MonoDroid] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.onDestroyView(FragmentContainer.java:57)
[MonoDroid] at android.support.v4.app.Fragment.performDestroyView(Fragment.java:2167)
[MonoDroid] at android.support.v4.app.FragmentManagerImpl.moveToState(FragmentManager.java:1100)
[MonoDroid] at android.support.v4.app.FragmentManagerImpl.detachFragment(FragmentManager.java:1363)
[MonoDroid] at android.support.v4.app.BackStackRecord.run(BackStackRecord.java:723)
[MonoDroid] at android.support.v4.app.FragmentManagerImpl.execPendingActions(FragmentManager.java:1572)
[MonoDroid] at android.support.v4.app.FragmentManagerImpl.executePendingTransactions(FragmentManager.java:545)
[MonoDroid] at android.support.v4.app.FragmentPagerAdapter.finishUpdate(FragmentPagerAdapter.java:141)
[MonoDroid] at android.support.v4.view.ViewPager.populate(ViewPager.java:1106)
[MonoDroid] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:552)
[MonoDroid] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:514)
[MonoDroid] at android.support.v4.view.ViewPager.setCurrentItem(ViewPager.java:506)
[MonoDroid] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.n_onTabSelected(Native Method)
[MonoDroid] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.onTabSelected(TabbedPageRenderer.java:86)
[MonoDroid] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:837)
[MonoDroid] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:809)
[MonoDroid] at android.support.design.widget.TabLayout$Tab.select(TabLayout.java:1077)
[MonoDroid] at android.support.design.widget.TabLayout$1.onClick(TabLayout.java:643)
[MonoDroid] at android.view.View.performClick(View.java:5204)
[MonoDroid] at android.view.View$PerformClick.run(View.java:21156)
[MonoDroid] at android.os.Handler.handleCallback(Handler.java:739)
[MonoDroid] at android.os.Handler.dispatchMessage(Handler.java:95)
[MonoDroid] at android.os.Looper.loop(Looper.java:148)
[MonoDroid] at android.app.ActivityThread.main(ActivityThread.java:5466)
[MonoDroid] at java.lang.reflect.Method.invoke(Native Method)
[MonoDroid] at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:726)
[MonoDroid] at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:616)
[art] art/runtime/java_vm_ext.cc:410] JNI DETECTED ERROR IN APPLICATION: JNI CallObjectMethod called with pending exception java.lang.IllegalStateException: Recursive entry to executePendingTransactions
[art] art/runtime/java_vm_ext.cc:410] at execPendingActions (FragmentManager.java:1544)
[art] art/runtime/java_vm_ext.cc:410] at executePendingTransactions (FragmentManager.java:545)
[art] art/runtime/java_vm_ext.cc:410] at n_onDestroyView (FragmentContainer.java:-2)
[art] art/runtime/java_vm_ext.cc:410] at onDestroyView (FragmentContainer.java:57)
[art] art/runtime/java_vm_ext.cc:410] at performDestroyView (Fragment.java:2167)
[art] art/runtime/java_vm_ext.cc:410] at moveToState (FragmentManager.java:1100)
[art] art/runtime/java_vm_ext.cc:410] at detachFragment (FragmentManager.java:1363)
[art] art/runtime/java_vm_ext.cc:410] at run (BackStackRecord.java:723)
[art] art/runtime/java_vm_ext.cc:410] at execPendingActions (FragmentManager.java:1572)
[art] art/runtime/java_vm_ext.cc:410] at executePendingTransactions (FragmentManager.java:545)
[art] art/runtime/java_vm_ext.cc:410] at finishUpdate (FragmentPagerAdapter.java:141)
[art] art/runtime/java_vm_ext.cc:410] at populate (ViewPager.java:1106)
[art] art/runtime/java_vm_ext.cc:410] at setCurrentItemInternal (ViewPager.java:552)
[art] art/runtime/java_vm_ext.cc:410] at setCurrentItemInternal (ViewPager.java:514)
[art] art/runtime/java_vm_ext.cc:410] at setCurrentItem (ViewPager.java:506)
[art] art/runtime/java_vm_ext.cc:410] at n_onTabSelected (TabbedPageRenderer.java:-2)
[art] art/runtime/java_vm_ext.cc:410] at onTabSelected (TabbedPageRenderer.java:86)
[art] art/runtime/java_vm_ext.cc:410] at selectTab (TabLayout.java:837)
[art] art/runtime/java_vm_ext.cc:410] at selectTab (TabLayout.java:809)
[art] art/runtime/java_vm_ext.cc:410] at select (TabLayout.java:1077)
[art] art/runtime/java_vm_ext.cc:410] at onClick (TabLayout.java:643)
[art] art/runtime/java_vm_ext.cc:410] at performClick (View.java:5204)
[art] art/runtime/java_vm_ext.cc:410] at run (View.java:21156)
[art] art/runtime/java_vm_ext.cc:410] at handleCallback (Handler.java:739)
[art] art/runtime/java_vm_ext.cc:410] at dispatchMessage (Handler.java:95)
[art] art/runtime/java_vm_ext.cc:410] at loop (Looper.java:148)
[art] art/runtime/java_vm_ext.cc:410] at main (ActivityThread.java:5466)
[art] art/runtime/java_vm_ext.cc:410] at invoke (Method.java:-2)
[art] art/runtime/java_vm_ext.cc:410] at run (ZygoteInit.java:726)
[art] art/runtime/java_vm_ext.cc:410] at main (ZygoteInit.java:616)
[art] art/runtime/java_vm_ext.cc:410]
[art] art/runtime/java_vm_ext.cc:410] in call to CallObjectMethod
[art] art/runtime/java_vm_ext.cc:410] from void md5270abb39e60627f0f200893b490a1ade.FragmentContainer.n_onDestroyView()
[art] art/runtime/java_vm_ext.cc:410] "main" prio=5 tid=1 Runnable
[art] art/runtime/java_vm_ext.cc:410] | group="main" sCount=0 dsCount=0 obj=0x743b93b0 self=0xb4d76500
[art] art/runtime/java_vm_ext.cc:410] | sysTid=11484 nice=0 cgrp=default sched=0/0 handle=0xb6ff0b38
[art] art/runtime/java_vm_ext.cc:410] | state=R schedstat=( 0 0 0 ) utm=205 stm=59 core=0 HZ=100
[art] art/runtime/java_vm_ext.cc:410] | stack=0xbe3da000-0xbe3dc000 stackSize=8MB
[art] art/runtime/java_vm_ext.cc:410] | held mutexes= "mutator lock"(shared held)
[art] art/runtime/java_vm_ext.cc:410] native: #00 pc 00370ce1 /system/lib/libart.so (art::DumpNativeStack(std::__1::basic_ostream<char, std::__1::char_traits<char> >&, int, char const*, art::ArtMethod*, void*)+160)
[art] art/runtime/java_vm_ext.cc:410] native: #01 pc 0035034f /system/lib/libart.so (art::Thread::Dump(std::__1::basic_ostream<char, std::__1::char_traits<char> >&) const+150)
[art] art/runtime/java_vm_ext.cc:410] native: #02 pc 0025a605 /system/lib/libart.so (art::JavaVMExt::JniAbort(char const*, char const*)+740)
[art] art/runtime/java_vm_ext.cc:410] native: #03 pc 0025acdd /system/lib/libart.so (art::JavaVMExt::JniAbortV(char const*, char const*, std::__va_list)+64)
[art] art/runtime/java_vm_ext.cc:410] native: #04 pc 000fd0b1 /system/lib/libart.so (art::ScopedCheck::AbortF(char const*, ...)+32)
[art] art/runtime/java_vm_ext.cc:410] native: #05 pc 001021c5 /system/lib/libart.so (art::ScopedCheck::Check(art::ScopedObjectAccess&, bool, char const*, art::JniValueType*) (.constprop.95)+5072)
[art] art/runtime/java_vm_ext.cc:410] native: #06 pc 00110a53 /system/lib/libart.so (art::CheckJNI::CallMethodV(char const*, _JNIEnv*, _jobject*, _jclass*, _jmethodID*, std::__va_list, art::Primitive::Type, art::InvokeType)+534)
[art] art/runtime/java_vm_ext.cc:410] native: #07 pc 001123d1 /system/lib/libart.so (art::CheckJNI::CallObjectMethod(_JNIEnv*, _jobject*, _jmethodID*, ...)+48)
[art] art/runtime/java_vm_ext.cc:410] native: #08 pc 0000b6b8 (???)
[art] art/runtime/java_vm_ext.cc:410] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.n_onDestroyView(Native method)
[art] art/runtime/java_vm_ext.cc:410] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.onDestroyView(FragmentContainer.java:57)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.Fragment.performDestroyView(Fragment.java:2167)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.FragmentManagerImpl.moveToState(FragmentManager.java:1100)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.FragmentManagerImpl.detachFragment(FragmentManager.java:1363)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.BackStackRecord.run(BackStackRecord.java:723)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.FragmentManagerImpl.execPendingActions(FragmentManager.java:1572)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.FragmentManagerImpl.executePendingTransactions(FragmentManager.java:545)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.app.FragmentPagerAdapter.finishUpdate(FragmentPagerAdapter.java:141)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.view.ViewPager.populate(ViewPager.java:1106)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:552)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:514)
[art] art/runtime/java_vm_ext.cc:410] at android.support.v4.view.ViewPager.setCurrentItem(ViewPager.java:506)
[art] art/runtime/java_vm_ext.cc:410] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.n_onTabSelected(Native method)
[art] art/runtime/java_vm_ext.cc:410] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.onTabSelected(TabbedPageRenderer.java:86)
[art] art/runtime/java_vm_ext.cc:410] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:837)
[art] art/runtime/java_vm_ext.cc:410] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:809)
[art] art/runtime/java_vm_ext.cc:410] at android.support.design.widget.TabLayout$Tab.select(TabLayout.java:1077)
[art] art/runtime/java_vm_ext.cc:410] at android.support.design.widget.TabLayout$1.onClick(TabLayout.java:643)
[art] art/runtime/java_vm_ext.cc:410] at android.view.View.performClick(View.java:5204)
[art] art/runtime/java_vm_ext.cc:410] at android.view.View$PerformClick.run(View.java:21156)
[art] art/runtime/java_vm_ext.cc:410] at android.os.Handler.handleCallback(Handler.java:739)
[art] art/runtime/java_vm_ext.cc:410] at android.os.Handler.dispatchMessage(Handler.java:95)
[art] art/runtime/java_vm_ext.cc:410] at android.os.Looper.loop(Looper.java:148)
[art] art/runtime/java_vm_ext.cc:410] at android.app.ActivityThread.main(ActivityThread.java:5466)
[art] art/runtime/java_vm_ext.cc:410] at java.lang.reflect.Method.invoke!(Native method)
[art] art/runtime/java_vm_ext.cc:410] at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:726)
[art] art/runtime/java_vm_ext.cc:410] at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:616)
[art] art/runtime/java_vm_ext.cc:410]
[art] art/runtime/runtime.cc:366] Runtime aborting...
[art] art/runtime/runtime.cc:366] Aborting thread:
[art] art/runtime/runtime.cc:366] "main" prio=5 tid=1 Native
[art] art/runtime/runtime.cc:366] | group="" sCount=0 dsCount=0 obj=0x743b93b0 self=0xb4d76500
[art] art/runtime/runtime.cc:366] | sysTid=11484 nice=0 cgrp=default sched=0/0 handle=0xb6ff0b38
[art] art/runtime/runtime.cc:366] | state=R schedstat=( 0 0 0 ) utm=206 stm=60 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xbe3da000-0xbe3dc000 stackSize=8MB
[art] art/runtime/runtime.cc:366] | held mutexes= "abort lock"
[art] art/runtime/runtime.cc:366] native: #00 pc 00370ce1 /system/lib/libart.so (art::DumpNativeStack(std::__1::basic_ostream<char, std::__1::char_traits<char> >&, int, char const*, art::ArtMethod*, void*)+160)
[art] art/runtime/runtime.cc:366] native: #01 pc 0035034f /system/lib/libart.so (art::Thread::Dump(std::__1::basic_ostream<char, std::__1::char_traits<char> >&) const+150)
[art] art/runtime/runtime.cc:366] native: #02 pc 00333707 /system/lib/libart.so (art::AbortState::DumpThread(std::__1::basic_ostream<char, std::__1::char_traits<char> >&, art::Thread*) const+26)
[art] art/runtime/runtime.cc:366] native: #03 pc 0033399f /system/lib/libart.so (art::Runtime::Abort()+562)
[art] art/runtime/runtime.cc:366] native: #04 pc 000f44db /system/lib/libart.so (art::LogMessage::~LogMessage()+2226)
[art] art/runtime/runtime.cc:366] native: #05 pc 0025a92f /system/lib/libart.so (art::JavaVMExt::JniAbort(char const*, char const*)+1550)
[art] art/runtime/runtime.cc:366] native: #06 pc 0025acdd /system/lib/libart.so (art::JavaVMExt::JniAbortV(char const*, char const*, std::__va_list)+64)
[art] art/runtime/runtime.cc:366] native: #07 pc 000fd0b1 /system/lib/libart.so (art::ScopedCheck::AbortF(char const*, ...)+32)
[art] art/runtime/runtime.cc:366] native: #08 pc 001021c5 /system/lib/libart.so (art::ScopedCheck::Check(art::ScopedObjectAccess&, bool, char const*, art::JniValueType*) (.constprop.95)+5072)
[art] art/runtime/runtime.cc:366] native: #09 pc 00110a53 /system/lib/libart.so (art::CheckJNI::CallMethodV(char const*, _JNIEnv*, _jobject*, _jclass*, _jmethodID*, std::__va_list, art::Primitive::Type, art::InvokeType)+534)
[art] art/runtime/runtime.cc:366] native: #10 pc 001123d1 /system/lib/libart.so (art::CheckJNI::CallObjectMethod(_JNIEnv*, _jobject*, _jmethodID*, ...)+48)
[art] art/runtime/runtime.cc:366] native: #11 pc 0000b6b8 (???)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.n_onDestroyView(Native method)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.onDestroyView(FragmentContainer.java:57)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.Fragment.performDestroyView(Fragment.java:2167)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.moveToState(FragmentManager.java:1100)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.detachFragment(FragmentManager.java:1363)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.BackStackRecord.run(BackStackRecord.java:723)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.execPendingActions(FragmentManager.java:1572)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.executePendingTransactions(FragmentManager.java:545)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentPagerAdapter.finishUpdate(FragmentPagerAdapter.java:141)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.populate(ViewPager.java:1106)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:552)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:514)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.setCurrentItem(ViewPager.java:506)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.n_onTabSelected(Native method)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.onTabSelected(TabbedPageRenderer.java:86)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:837)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:809)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout$Tab.select(TabLayout.java:1077)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout$1.onClick(TabLayout.java:643)
[art] art/runtime/runtime.cc:366] at android.view.View.performClick(View.java:5204)
[art] art/runtime/runtime.cc:366] at android.view.View$PerformClick.run(View.java:21156)
[art] art/runtime/runtime.cc:366] at android.os.Handler.handleCallback(Handler.java:739)
[art] art/runtime/runtime.cc:366] at android.os.Handler.dispatchMessage(Handler.java:95)
[art] art/runtime/runtime.cc:366] at android.os.Looper.loop(Looper.java:148)
[art] art/runtime/runtime.cc:366] at android.app.ActivityThread.main(ActivityThread.java:5466)
[art] art/runtime/runtime.cc:366] at java.lang.reflect.Method.invoke!(Native method)
[art] art/runtime/runtime.cc:366] at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:726)
[art] art/runtime/runtime.cc:366] at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:616)
[art] art/runtime/runtime.cc:366] Pending exception java.lang.IllegalStateException: Recursive entry to executePendingTransactions
[art] art/runtime/runtime.cc:366] at execPendingActions (FragmentManager.java:1544)
[art] art/runtime/runtime.cc:366] at executePendingTransactions (FragmentManager.java:545)
[art] art/runtime/runtime.cc:366] at n_onDestroyView (FragmentContainer.java:-2)
[art] art/runtime/runtime.cc:366] at onDestroyView (FragmentContainer.java:57)
[art] art/runtime/runtime.cc:366] at performDestroyView (Fragment.java:2167)
[art] art/runtime/runtime.cc:366] at moveToState (FragmentManager.java:1100)
[art] art/runtime/runtime.cc:366] at detachFragment (FragmentManager.java:1363)
[art] art/runtime/runtime.cc:366] at run (BackStackRecord.java:723)
[art] art/runtime/runtime.cc:366] at execPendingActions (FragmentManager.java:1572)
[art] art/runtime/runtime.cc:366] at executePendingTransactions (FragmentManager.java:545)
[art] art/runtime/runtime.cc:366] at finishUpdate (FragmentPagerAdapter.java:141)
[art] art/runtime/runtime.cc:366] at populate (ViewPager.java:1106)
[art] art/runtime/runtime.cc:366] at setCurrentItemInternal (ViewPager.java:552)
[art] art/runtime/runtime.cc:366] at setCurrentItemInternal (ViewPager.java:514)
[art] art/runtime/runtime.cc:366] at setCurrentItem (ViewPager.java:506)
[art] art/runtime/runtime.cc:366] at n_onTabSelected (TabbedPageRenderer.java:-2)
[art] art/runtime/runtime.cc:366] at onTabSelected (TabbedPageRenderer.java:86)
[art] art/runtime/runtime.cc:366] at selectTab (TabLayout.java:837)
[art] art/runtime/runtime.cc:366] at selectTab (TabLayout.java:809)
[art] art/runtime/runtime.cc:366] at select (TabLayout.java:1077)
[art] art/runtime/runtime.cc:366] at onClick (TabLayout.java:643)
[art] art/runtime/runtime.cc:366] at performClick (View.java:5204)
[art] art/runtime/runtime.cc:366] at run (View.java:21156)
[art] art/runtime/runtime.cc:366] at handleCallback (Handler.java:739)
[art] art/runtime/runtime.cc:366] at dispatchMessage (Handler.java:95)
[art] art/runtime/runtime.cc:366] at loop (Looper.java:148)
[art] art/runtime/runtime.cc:366] at main (ActivityThread.java:5466)
[art] art/runtime/runtime.cc:366] at invoke (Method.java:-2)
[art] art/runtime/runtime.cc:366] at run (ZygoteInit.java:726)
[art] art/runtime/runtime.cc:366] at main (ZygoteInit.java:616)
[art] art/runtime/runtime.cc:366] Dumping all threads without appropriate locks held: thread list lock mutator lock
[art] art/runtime/runtime.cc:366] All threads:
[art] art/runtime/runtime.cc:366] DALVIK THREADS (18):
[art] art/runtime/runtime.cc:366] "main" prio=5 tid=1 Runnable
[art] art/runtime/runtime.cc:366] | group="" sCount=0 dsCount=0 obj=0x743b93b0 self=0xb4d76500
[art] art/runtime/runtime.cc:366] | sysTid=11484 nice=0 cgrp=default sched=0/0 handle=0xb6ff0b38
[art] art/runtime/runtime.cc:366] | state=R schedstat=( 0 0 0 ) utm=209 stm=60 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xbe3da000-0xbe3dc000 stackSize=8MB
[art] art/runtime/runtime.cc:366] | held mutexes= "abort lock" "mutator lock"(shared held)
[art] art/runtime/runtime.cc:366] native: #00 pc 00370ce1 /system/lib/libart.so (art::DumpNativeStack(std::__1::basic_ostream<char, std::__1::char_traits<char> >&, int, char const*, art::ArtMethod*, void*)+160)
[art] art/runtime/runtime.cc:366] native: #01 pc 0035034f /system/lib/libart.so (art::Thread::Dump(std::__1::basic_ostream<char, std::__1::char_traits<char> >&) const+150)
[art] art/runtime/runtime.cc:366] native: #02 pc 0035a253 /system/lib/libart.so (art::DumpCheckpoint::Run(art::Thread*)+442)
[art] art/runtime/runtime.cc:366] native: #03 pc 0035ae11 /system/lib/libart.so (art::ThreadList::RunCheckpoint(art::Closure*)+212)
[art] art/runtime/runtime.cc:366] native: #04 pc 0035b33f /system/lib/libart.so (art::ThreadList::Dump(std::__1::basic_ostream<char, std::__1::char_traits<char> >&)+142)
[art] art/runtime/runtime.cc:366] native: #05 pc 00333915 /system/lib/libart.so (art::Runtime::Abort()+424)
[art] art/runtime/runtime.cc:366] native: #06 pc 000f44db /system/lib/libart.so (art::LogMessage::~LogMessage()+2226)
[art] art/runtime/runtime.cc:366] native: #07 pc 0025a92f /system/lib/libart.so (art::JavaVMExt::JniAbort(char const*, char const*)+1550)
[art] art/runtime/runtime.cc:366] native: #08 pc 0025acdd /system/lib/libart.so (art::JavaVMExt::JniAbortV(char const*, char const*, std::__va_list)+64)
[art] art/runtime/runtime.cc:366] native: #09 pc 000fd0b1 /system/lib/libart.so (art::ScopedCheck::AbortF(char const*, ...)+32)
[art] art/runtime/runtime.cc:366] native: #10 pc 001021c5 /system/lib/libart.so (art::ScopedCheck::Check(art::ScopedObjectAccess&, bool, char const*, art::JniValueType*) (.constprop.95)+5072)
[art] art/runtime/runtime.cc:366] native: #11 pc 00110a53 /system/lib/libart.so (art::CheckJNI::CallMethodV(char const*, _JNIEnv*, _jobject*, _jclass*, _jmethodID*, std::__va_list, art::Primitive::Type, art::InvokeType)+534)
[art] art/runtime/runtime.cc:366] native: #12 pc 001123d1 /system/lib/libart.so (art::CheckJNI::CallObjectMethod(_JNIEnv*, _jobject*, _jmethodID*, ...)+48)
[art] art/runtime/runtime.cc:366] native: #13 pc 0000b6b8 (???)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.n_onDestroyView(Native method)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.FragmentContainer.onDestroyView(FragmentContainer.java:57)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.Fragment.performDestroyView(Fragment.java:2167)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.moveToState(FragmentManager.java:1100)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.detachFragment(FragmentManager.java:1363)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.BackStackRecord.run(BackStackRecord.java:723)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.execPendingActions(FragmentManager.java:1572)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentManagerImpl.executePendingTransactions(FragmentManager.java:545)
[art] art/runtime/runtime.cc:366] at android.support.v4.app.FragmentPagerAdapter.finishUpdate(FragmentPagerAdapter.java:141)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.populate(ViewPager.java:1106)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:552)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.setCurrentItemInternal(ViewPager.java:514)
[art] art/runtime/runtime.cc:366] at android.support.v4.view.ViewPager.setCurrentItem(ViewPager.java:506)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.n_onTabSelected(Native method)
[art] art/runtime/runtime.cc:366] at md5270abb39e60627f0f200893b490a1ade.TabbedPageRenderer.onTabSelected(TabbedPageRenderer.java:86)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:837)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout.selectTab(TabLayout.java:809)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout$Tab.select(TabLayout.java:1077)
[art] art/runtime/runtime.cc:366] at android.support.design.widget.TabLayout$1.onClick(TabLayout.java:643)
[art] art/runtime/runtime.cc:366] at android.view.View.performClick(View.java:5204)
[art] art/runtime/runtime.cc:366] at android.view.View$PerformClick.run(View.java:21156)
[art] art/runtime/runtime.cc:366] at android.os.Handler.handleCallback(Handler.java:739)
[art] art/runtime/runtime.cc:366] at android.os.Handler.dispatchMessage(Handler.java:95)
[art] art/runtime/runtime.cc:366] at android.os.Looper.loop(Looper.java:148)
[art] art/runtime/runtime.cc:366] at android.app.ActivityThread.main(ActivityThread.java:5466)
[art] art/runtime/runtime.cc:366] at java.lang.reflect.Method.invoke!(Native method)
[art] art/runtime/runtime.cc:366] at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:726)
[art] art/runtime/runtime.cc:366] at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:616)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "Signal Catcher" prio=5 tid=2 WaitingInMainSignalCatcherLoop
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c290a0 self=0xaedb7000
[art] art/runtime/runtime.cc:366] | sysTid=11489 nice=0 cgrp=default sched=0/0 handle=0xb43c7930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=3 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb42cb000-0xb42cd000 stackSize=1014KB
[art] art/runtime/runtime.cc:366] | held mutexes=
[art] art/runtime/runtime.cc:366] native: #00 pc 000485e0 /system/lib/libc.so (__rt_sigtimedwait+12)
[art] art/runtime/runtime.cc:366] native: #01 pc 0001c421 /system/lib/libc.so (sigwait+24)
[art] art/runtime/runtime.cc:366] native: #02 pc 0033a605 /system/lib/libart.so (art::SignalCatcher::WaitForSignal(art::Thread*, art::SignalSet&)+76)
[art] art/runtime/runtime.cc:366] native: #03 pc 0033b767 /system/lib/libart.so (art::SignalCatcher::Run(void*)+218)
[art] art/runtime/runtime.cc:366] native: #04 pc 00046ea5 /system/lib/libc.so (__pthread_start(void*)+32)
[art] art/runtime/runtime.cc:366] native: #05 pc 00018c35 /system/lib/libc.so (__start_thread+8)
[art] art/runtime/runtime.cc:366] (no managed stack frames)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "JDWP" prio=5 tid=3 WaitingInMainDebuggerLoop
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c2b0a0 self=0xacb58800
[art] art/runtime/runtime.cc:366] | sysTid=11490 nice=0 cgrp=default sched=0/0 handle=0xb42c3930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb41c7000-0xb41c9000 stackSize=1014KB
[art] art/runtime/runtime.cc:366] | held mutexes=
[art] art/runtime/runtime.cc:366] native: #00 pc 000494f4 /system/lib/libc.so (recvmsg+8)
[art] art/runtime/runtime.cc:366] native: #01 pc 0040103f /system/lib/libart.so (art::JDWP::JdwpAdbState::ReceiveClientFd()+94)
[art] art/runtime/runtime.cc:366] native: #02 pc 00401801 /system/lib/libart.so (art::JDWP::JdwpAdbState::Accept()+104)
[art] art/runtime/runtime.cc:366] native: #03 pc 00266cdb /system/lib/libart.so (art::JDWP::JdwpState::Run()+238)
[art] art/runtime/runtime.cc:366] native: #04 pc 00267ba5 /system/lib/libart.so (art::JDWP::StartJdwpThread(void*)+16)
[art] art/runtime/runtime.cc:366] native: #05 pc 00046ea5 /system/lib/libc.so (__pthread_start(void*)+32)
[art] art/runtime/runtime.cc:366] native: #06 pc 00018c35 /system/lib/libc.so (__start_thread+8)
[art] art/runtime/runtime.cc:366] (no managed stack frames)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "ReferenceQueueDaemon" prio=5 tid=4 Waiting
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c27640 self=0xacb59c00
[art] art/runtime/runtime.cc:366] | sysTid=11491 nice=0 cgrp=default sched=0/0 handle=0xb41bd930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb40bb000-0xb40bd000 stackSize=1038KB
[art] art/runtime/runtime.cc:366] | held mutexes=
[art] art/runtime/runtime.cc:366] native: #00 pc 00016610 /system/lib/libc.so (syscall+28)
[art] art/runtime/runtime.cc:366] native: #01 pc 000f6a29 /system/lib/libart.so (art::ConditionVariable::Wait(art::Thread*)+96)
[art] art/runtime/runtime.cc:366] native: #02 pc 002beb69 /system/lib/libart.so (art::Monitor::Wait(art::Thread*, long long, int, bool, art::ThreadState)+1144)
[art] art/runtime/runtime.cc:366] native: #03 pc 002bf8c7 /system/lib/libart.so (art::Monitor::Wait(art::Thread*, art::mirror::Object*, long long, int, bool, art::ThreadState)+142)
[art] art/runtime/runtime.cc:366] native: #04 pc 002d111b /system/lib/libart.so (art::Object_wait(_JNIEnv*, _jobject*)+38)
[art] art/runtime/runtime.cc:366] native: #05 pc 0000037f /data/dalvik-cache/arm/system@framework@boot.oat (Java_java_lang_Object_wait__+74)
[art] art/runtime/runtime.cc:366] at java.lang.Object.wait!(Native method)
[art] art/runtime/runtime.cc:366] - waiting on <0x03177f15> (a java.lang.Class<java.lang.ref.ReferenceQueue>)
[art] art/runtime/runtime.cc:366] at java.lang.Daemons$ReferenceQueueDaemon.run(Daemons.java:147)
[art] art/runtime/runtime.cc:366] - locked <0x03177f15> (a java.lang.Class<java.lang.ref.ReferenceQueue>)
[art] art/runtime/runtime.cc:366] at java.lang.Thread.run(Thread.java:818)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "FinalizerDaemon" prio=5 tid=5 Waiting
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c276a0 self=0xacb5a100
[art] art/runtime/runtime.cc:366] | sysTid=11492 nice=0 cgrp=default sched=0/0 handle=0xb40b3930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=3 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb3fb1000-0xb3fb3000 stackSize=1038KB
[art] art/runtime/runtime.cc:366] | held mutexes=
[art] art/runtime/runtime.cc:366] native: #00 pc 00016610 /system/lib/libc.so (syscall+28)
[art] art/runtime/runtime.cc:366] native: #01 pc 000f6a29 /system/lib/libart.so (art::ConditionVariable::Wait(art::Thread*)+96)
[art] art/runtime/runtime.cc:366] native: #02 pc 002beb69 /system/lib/libart.so (art::Monitor::Wait(art::Thread*, long long, int, bool, art::ThreadState)+1144)
[art] art/runtime/runtime.cc:366] native: #03 pc 002bf8c7 /system/lib/libart.so (art::Monitor::Wait(art::Thread*, art::mirror::Object*, long long, int, bool, art::ThreadState)+142)
[art] art/runtime/runtime.cc:366] native: #04 pc 002d1155 /system/lib/libart.so (art::Object_waitJI(_JNIEnv*, _jobject*, long long, int)+44)
[art] art/runtime/runtime.cc:366] native: #05 pc 0000056d /data/dalvik-cache/arm/system@framework@boot.oat (Java_java_lang_Object_wait__JI+96)
[art] art/runtime/runtime.cc:366] at java.lang.Object.wait!(Native method)
[art] art/runtime/runtime.cc:366] - waiting on <0x08f7b22a> (a java.lang.ref.ReferenceQueue)
[art] art/runtime/runtime.cc:366] at java.lang.Object.wait(Object.java:423)
[art] art/runtime/runtime.cc:366] at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:101)
[art] art/runtime/runtime.cc:366] - locked <0x08f7b22a> (a java.lang.ref.ReferenceQueue)
[art] art/runtime/runtime.cc:366] at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:72)
[art] art/runtime/runtime.cc:366] at java.lang.Daemons$FinalizerDaemon.run(Daemons.java:185)
[art] art/runtime/runtime.cc:366] at java.lang.Thread.run(Thread.java:818)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "FinalizerWatchdogDaemon" prio=5 tid=6 Waiting
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c27700 self=0xacb5a600
[art] art/runtime/runtime.cc:366] | sysTid=11493 nice=0 cgrp=default sched=0/0 handle=0xb3fa9930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb3ea7000-0xb3ea9000 stackSize=1038KB
[art] art/runtime/runtime.cc:366] | held mutexes=
[art] art/runtime/runtime.cc:366] native: #00 pc 00016610 /system/lib/libc.so (syscall+28)
[art] art/runtime/runtime.cc:366] native: #01 pc 000f6a29 /system/lib/libart.so (art::ConditionVariable::Wait(art::Thread*)+96)
[art] art/runtime/runtime.cc:366] native: #02 pc 002beb69 /system/lib/libart.so (art::Monitor::Wait(art::Thread*, long long, int, bool, art::ThreadState)+1144)
[art] art/runtime/runtime.cc:366] native: #03 pc 002bf8c7 /system/lib/libart.so (art::Monitor::Wait(art::Thread*, art::mirror::Object*, long long, int, bool, art::ThreadState)+142)
[art] art/runtime/runtime.cc:366] native: #04 pc 002d111b /system/lib/libart.so (art::Object_wait(_JNIEnv*, _jobject*)+38)
[art] art/runtime/runtime.cc:366] native: #05 pc 0000037f /data/dalvik-cache/arm/system@framework@boot.oat (Java_java_lang_Object_wait__+74)
[art] art/runtime/runtime.cc:366] at java.lang.Object.wait!(Native method)
[art] art/runtime/runtime.cc:366] - waiting on <0x003bd81b> (a java.lang.Daemons$FinalizerWatchdogDaemon)
[art] art/runtime/runtime.cc:366] at java.lang.Daemons$FinalizerWatchdogDaemon.waitForObject(Daemons.java:255)
[art] art/runtime/runtime.cc:366] - locked <0x003bd81b> (a java.lang.Daemons$FinalizerWatchdogDaemon)
[art] art/runtime/runtime.cc:366] at java.lang.Daemons$FinalizerWatchdogDaemon.run(Daemons.java:227)
[art] art/runtime/runtime.cc:366] at java.lang.Thread.run(Thread.java:818)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "HeapTaskDaemon" prio=5 tid=7 Blocked
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c27760 self=0xacb5ab00
[art] art/runtime/runtime.cc:366] | sysTid=11494 nice=0 cgrp=default sched=0/0 handle=0xb3e9f930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb3d9d000-0xb3d9f000 stackSize=1038KB
[art] art/runtime/runtime.cc:366] | held mutexes=
[art] art/runtime/runtime.cc:366] native: #00 pc 00016610 /system/lib/libc.so (syscall+28)
[art] art/runtime/runtime.cc:366] native: #01 pc 000f6a29 /system/lib/libart.so (art::ConditionVariable::Wait(art::Thread*)+96)
[art] art/runtime/runtime.cc:366] native: #02 pc 001d72d5 /system/lib/libart.so (art::gc::TaskProcessor::GetTask(art::Thread*)+104)
[art] art/runtime/runtime.cc:366] native: #03 pc 001d77e7 /system/lib/libart.so (art::gc::TaskProcessor::RunAllTasks(art::Thread*)+38)
[art] art/runtime/runtime.cc:366] native: #04 pc 0000037f /data/dalvik-cache/arm/system@framework@boot.oat (Java_dalvik_system_VMRuntime_runHeapTasks__+74)
[art] art/runtime/runtime.cc:366] at dalvik.system.VMRuntime.runHeapTasks(Native method)
[art] art/runtime/runtime.cc:366] - waiting to lock an unknown object
[art] art/runtime/runtime.cc:366] at java.lang.Daemons$HeapTaskDaemon.run(Daemons.java:355)
[art] art/runtime/runtime.cc:366] at java.lang.Thread.run(Thread.java:818)
[art] art/runtime/runtime.cc:366]
[art] art/runtime/runtime.cc:366] "Binder_1" prio=5 tid=8 Native
[art] art/runtime/runtime.cc:366] | group="" sCount=1 dsCount=0 obj=0x12c320a0 self=0xaedb8400
[art] art/runtime/runtime.cc:366] | sysTid=11495 nice=0 cgrp=default sched=0/0 handle=0xb3c99930
[art] art/runtime/runtime.cc:366] | state=S schedstat=( 0 0 0 ) utm=0 stm=0 core=0 HZ=100
[art] art/runtime/runtime.cc:366] | stack=0xb3b9d000-0xb3b9f000 stackSize=1014KB
[libc] Fatal signal 6 (SIGABRT), code -6 in tid 11484 (nav_inside_tabs)
```